package net.dev909.chocolatesprinkle.data;

import net.dev909.chocolatesprinkle.proxy.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = References.MOD_ID, name = References.MOD_NAME, version = References.MOD_VERSION)
public class ChocolateSprinkleMod {
	
	@SidedProxy(clientSide = References.CLIENT_PROXY, serverSide = References.SERVER_PROXY)
	public static CommonProxy proxy;
	
	public static CreativeTabs csCreativeTab = new CSCreativeTab(CreativeTabs.getNextID(), "CS_Creative_Tab");
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		CSConfig config = new CSConfig(event);
		
		config.loadConfig();
		config.initializeVariables();
		config.saveConfig();
		
		CSItems.init();
		CSItems.register();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.registerRenders();
		//Register recipes
		CSCraftingManager.registerCraftingRecipes();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		
	}
}
