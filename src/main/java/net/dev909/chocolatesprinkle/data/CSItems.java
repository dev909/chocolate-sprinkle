package net.dev909.chocolatesprinkle.data;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemSword;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CSItems {
	
	static Tools tools;
	
	public static Item emeraldSword;
	public static Item emeraldPickaxe;
	public static Item emeraldAxe;
	public static Item emeraldHoe;
	public static Item emeraldShovel;
	
	public static Item emeraldHelmet;
	public static Item emeraldChest;
	public static Item emeraldLegs;
	public static Item emeraldBoots;
	
	public static Item obsidianSword;
	public static Item obsidianPickaxe;
	public static Item obsidianAxe;
	public static Item obsidianHoe;
	public static Item obsidianShovel;
	
	public static Item obsidianHelmet;
	public static Item obsidianChest;
	public static Item obsidianLegs;
	public static Item obsidianBoots;
	
	/*
    //Ratios from material to diamond
    private final static float DIAMOND_ATTACK_DAMAGE = 6.0f;
    private final static float DIAMOND_ATTACK_SPEED = -3.0f;
    
    private final static float EMERALD_DAMAGE_RATIO = 0.777f;
    private final static float EMERALD_SPEED_RATIO = 1.5f;
    
    private final static float OBSIDIAN_DAMAGE_RATIO = 1.33f;
    private final static float OBSIDIAN_SPEED_RATIO = 0.5f;
	*/
	
	//harvestLevel, maxUses, efficientcyOnProperMaterial, damageVsEntity, enchantibility
    public static final Item.ToolMaterial CST_EMERALD = new EnumHelper().addToolMaterial("CST_EMERALD", 3, 1000, 12.0F, 3.0F, 22);
    public static final Item.ToolMaterial CST_OBSIDIAN = new EnumHelper().addToolMaterial("CST_OBSIDIAN", 3, 1800, 7.0F, 8.0F, 10);
    
    //Ratios should be material/diamond
    //Emerald damage = 0.777, speed = 1.5
    //Obsidian damage = 1.333, speed = 0.5
    //Pickaxe damage is material#damageVsEntity + modifier
    //Pickaxe, axe, sword, hoe, shovel
    private final static float[] EMERALD_DAMAGE  = {-0.2f, 6.0f};
    private final static float[] EMERALD_SPEED  = {-2.8f, -2.5f};
    private final static float[] OBSIDIAN_DAMAGE  = {1.0f, 11.0f};
    private final static float[] OBSIDIAN_SPEED  = {-2.8f, -3.5f};
    
    /*
	private final static float EMERALD_ATTACK_DAMAGE = 6.0f;  // 7
	private final static float EMERALD_ATTACK_SPEED = -2.5f;  // 1.5
	
	private final static float OBSIDIAN_ATTACK_DAMAGE = 11.0f; // 12
	private final static float OBSIDIAN_ATTACK_SPEED = -3.5f;  // 0.5
    */
    
    public static final ItemArmor.ArmorMaterial CSA_EMERALD = new EnumHelper().addArmorMaterial("CSA_EMERALD", References.MOD_ID + ":" + "CSAemerald", 21, new int[]{3, 8, 6, 3}, 22, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 0);
    public static final ItemArmor.ArmorMaterial CSA_OBSIDIAN = new EnumHelper().addArmorMaterial("CSA_OBSIDIAN", References.MOD_ID + ":" + "CSAobsidian", 38, new int[]{3, 8, 6, 3}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 0);
    
	
	public static void init() {
			emeraldSword = new ItemSword(CST_EMERALD).setUnlocalizedName("emeraldSword").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			emeraldPickaxe = new CSToolPickaxe(CST_EMERALD, EMERALD_DAMAGE[tools.PICKAXE.ordinal()], EMERALD_SPEED[tools.PICKAXE.ordinal()]).setUnlocalizedName("emeraldPickaxe").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			emeraldAxe = new CSToolAxe(CST_EMERALD, EMERALD_DAMAGE[tools.AXE.ordinal()], EMERALD_SPEED[tools.AXE.ordinal()]).setUnlocalizedName("emeraldAxe").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			emeraldHoe = new ItemHoe(CST_EMERALD).setUnlocalizedName("emeraldHoe").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			emeraldShovel = new ItemSpade(CST_EMERALD).setUnlocalizedName("emeraldShovel").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			
			emeraldHelmet = new CSArmor(CSA_EMERALD, 1, EntityEquipmentSlot.HEAD).setUnlocalizedName("emeraldHelmet").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			emeraldChest = new CSArmor(CSA_EMERALD, 1, EntityEquipmentSlot.CHEST).setUnlocalizedName("emeraldChest").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			emeraldLegs = new CSArmor(CSA_EMERALD, 2, EntityEquipmentSlot.LEGS).setUnlocalizedName("emeraldLegs").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			emeraldBoots = new CSArmor(CSA_EMERALD, 1, EntityEquipmentSlot.FEET).setUnlocalizedName("emeraldBoots").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			
			obsidianSword = new ItemSword(CST_OBSIDIAN).setUnlocalizedName("obsidianSword").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			obsidianPickaxe = new CSToolPickaxe(CST_OBSIDIAN, OBSIDIAN_DAMAGE[tools.PICKAXE.ordinal()], OBSIDIAN_SPEED[tools.PICKAXE.ordinal()]).setUnlocalizedName("obsidianPickaxe").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			obsidianAxe = new CSToolAxe(CST_OBSIDIAN, OBSIDIAN_DAMAGE[tools.AXE.ordinal()], OBSIDIAN_SPEED[tools.AXE.ordinal()]).setUnlocalizedName("obsidianAxe").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			obsidianHoe = new ItemHoe(CST_OBSIDIAN).setUnlocalizedName("obsidianHoe").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			obsidianShovel = new ItemSpade(CST_OBSIDIAN).setUnlocalizedName("obsidianShovel").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			
			obsidianHelmet = new CSArmor(CSA_OBSIDIAN, 1, EntityEquipmentSlot.HEAD).setUnlocalizedName("obsidianHelmet").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			obsidianChest = new CSArmor(CSA_OBSIDIAN, 1, EntityEquipmentSlot.CHEST).setUnlocalizedName("obsidianChest").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			obsidianLegs = new CSArmor(CSA_OBSIDIAN, 2, EntityEquipmentSlot.LEGS).setUnlocalizedName("obsidianLegs").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
			obsidianBoots = new CSArmor(CSA_OBSIDIAN, 1, EntityEquipmentSlot.FEET).setUnlocalizedName("obsidianBoots").setCreativeTab(ChocolateSprinkleMod.csCreativeTab);
	}
	
	public static void register() {
		GameRegistry.registerItem(emeraldSword, emeraldSword.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(emeraldPickaxe, emeraldPickaxe.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(emeraldAxe, emeraldAxe.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(emeraldHoe, emeraldHoe.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(emeraldShovel, emeraldShovel.getUnlocalizedName().substring(5));

		GameRegistry.registerItem(emeraldHelmet, emeraldHelmet.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(emeraldChest, emeraldChest.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(emeraldLegs, emeraldLegs.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(emeraldBoots, emeraldBoots.getUnlocalizedName().substring(5));
		
		GameRegistry.registerItem(obsidianSword, obsidianSword.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(obsidianPickaxe, obsidianPickaxe.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(obsidianAxe, obsidianAxe.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(obsidianHoe, obsidianHoe.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(obsidianShovel, obsidianShovel.getUnlocalizedName().substring(5));

		GameRegistry.registerItem(obsidianHelmet, obsidianHelmet.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(obsidianChest, obsidianChest.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(obsidianLegs, obsidianLegs.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(obsidianBoots, obsidianBoots.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders() {
		registerItemRender(emeraldSword);
		registerItemRender(emeraldPickaxe);
		registerItemRender(emeraldAxe);
		registerItemRender(emeraldHoe);
		registerItemRender(emeraldShovel);
		
		registerItemRender(emeraldHelmet);
		registerItemRender(emeraldChest);
		registerItemRender(emeraldLegs);
		registerItemRender(emeraldBoots);
		
		registerItemRender(obsidianSword);
		registerItemRender(obsidianPickaxe);
		registerItemRender(obsidianAxe);
		registerItemRender(obsidianHoe);
		registerItemRender(obsidianShovel);
		
		registerItemRender(obsidianHelmet);
		registerItemRender(obsidianChest);
		registerItemRender(obsidianLegs);
		registerItemRender(obsidianBoots);
	}
	
	public static void registerItemRender(Item item) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(References.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	private enum Tools {
		PICKAXE,
		AXE,
		SWORD,
		HOE,
		SHOVEL;	
	}
}
