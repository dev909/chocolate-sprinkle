package net.dev909.chocolatesprinkle.data;

public class References {
	public static final String MOD_ID = "chocolatesprinkle";
	public static final String MOD_NAME = "Chocolate Sprinkle";
	public static final String MOD_VERSION = "1.0";
	public static final String CLIENT_PROXY = "net.dev909.chocolatesprinkle.proxy.ClientProxy";
	public static final String SERVER_PROXY = "net.dev909.chocolatesprinkle.proxy.CommonProxy";
}
