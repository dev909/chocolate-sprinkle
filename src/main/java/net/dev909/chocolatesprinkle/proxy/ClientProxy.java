package net.dev909.chocolatesprinkle.proxy;

import net.dev909.chocolatesprinkle.data.CSItems;

public class ClientProxy extends CommonProxy{
	@Override
	public void registerRenders() {
		CSItems.registerRenders();
	}
}
